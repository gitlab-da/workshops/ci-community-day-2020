# GitLab CI Community Day

## Resources

- [Event](https://www.meetup.com/gitlab-virtual-meetups/)
- [Slides](https://docs.google.com/presentation/d/1oJLEL2OVm0gz8qvIlqpoidGdmsGt1exT2pwbZjaVTaE/edit?usp=sharing)
- Recordings
  - [Session 1 with Abubakar Siddiq Ango](https://www.youtube.com/watch?v=avPF9Ds6eCI)
  - [Session 2 with Michael Friedrich](https://www.youtube.com/watch?v=yAfrT09ETKg)
  - [Session 3 with Brendan O'Leary](https://www.youtube.com/watch?v=9PWttQH-GG4)

The slides provide the step-by-step instructions as exercises for this repository.

- CI/CD Getting Started
- Security scanning
